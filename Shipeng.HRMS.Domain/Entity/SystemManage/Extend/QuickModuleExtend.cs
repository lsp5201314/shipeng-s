
namespace Shipeng.HRMS.Domain.SystemManage
{
    /// <summary>
    /// QuickModule Entity Model
    /// </summary>
    public class QuickModuleExtend
    {
        public string id { get; set; }
        public string title { get; set; }
        public string href { get; set; }
        public string icon { get; set; }

    }
}