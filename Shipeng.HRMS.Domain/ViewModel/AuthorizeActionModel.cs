﻿
namespace Shipeng.HRMS.Domain.ViewModel
{
    public class AuthorizeActionModel
    {
        public string F_Id { set; get; }
        public string F_UrlAddress { set; get; }
        public string F_Authorize { get; set; }
    }
}
