﻿using AutoMapper;

namespace Shipeng.Application
{
    public class ApplicationAutoMapperProfile : Profile
    {
        /* 可以在此处配置自动映射配置
         * 或者，可以拆分映射配置
         * 分为多个配置文件类，以实现更好的组织
         */
    }
}
