﻿using Mapster;
using Shipeng.Application.Contracts;
using Shipeng.Application.Contracts.Dtos.Node;
using Shipeng.Domain.Entities;
using Shipeng.Domain.Node;
using Volo.Abp.Domain.Repositories;
namespace Shipeng.Application
{
    public class NodeAppService : BaseApplicationService, INodeAppService
    {
        private readonly IRepository<Node> _repository;
        private readonly INodeManager _nodeManager;

        public NodeAppService(IRepository<Node> repository, INodeManager nodeManager)
        {
            _repository = repository;
            _nodeManager = nodeManager;
        }

        public async Task<ShipengResult> CreateAsync(CreateNodeDto createNode)
        {
            createNode.Server = createNode.Server.TrimEnd('/');
            var model = await _nodeManager.CreateAsync(createNode.NodeName, createNode.Server);
            TypeAdapter.Adapt(createNode, model);
            await _repository.InsertAsync(model);
            return Ok();
        }

        public async Task<ShipengResult> DeleteAsync(Guid id)
        {
            await _repository.DeleteAsync(x => x.Id == id);
            return Ok();
        }

        public async Task<ShipengResult<List<NodeDto>>> GetAllAsync()
        {
            var result = (await _repository.GetQueryableAsync())
                .OrderByDescending(x => x.Created)
                .ProjectToType<NodeDto>()
                .ToList();
            return Ok(result);
        }

        public async Task<ShipengResult<NodeDto>> GetAsync(Guid id)
        {
            var result = (await _repository.GetQueryableAsync())
                .Where(x => x.Id == id)
                .ProjectToType<NodeDto>()
                .FirstOrDefault();
            return Ok(result);
        }

        public async Task<ShipengPageResult<List<NodeDto>>> GetListAsync(int page = 1, int pageSize = 10)
        {
            var query = await _repository.GetQueryableAsync();
            var result = query
                .OrderByDescending(x => x.Created)
                .PageBy((page - 1) * pageSize, pageSize)
                .ProjectToType<NodeDto>()
                .ToList();
            return Ok(result, query.Count());
        }

        public async Task<ShipengResult> UpdateAsync(UpdateNodeDto updateNode)
        {
            updateNode.Server = updateNode.Server.TrimEnd('/');
            var model = await _nodeManager.UpdateAsync(updateNode.Id, updateNode.NodeName, updateNode.Server);
            TypeAdapter.Adapt(updateNode, model);
            model.Updated = DateTime.Now;
            await _repository.UpdateAsync(model);
            return Ok();
        }
    }
}
