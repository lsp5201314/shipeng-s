using System.Reflection;

namespace Shipeng.Util
{
    public class FormUtil
    {
        /// <summary>
        /// 获取值
        /// </summary>
        /// <param name="form">The form.</param>
        /// <returns>System.String.</returns>
        public static List<string> SetValue(string content)
        {
            //List<FormValue> list = JsonHelper.ToObject<List<FormValue>>(content);
            List<FormValue> list = content.ToObject<List<FormValue>>();
            List<string> temp = new List<string>();
            SetFormValue(list, temp);
            return temp;
        }

        private static List<string> SetFormValue(List<FormValue> list, List<string> temp)
        {
            foreach (var item in list)
            {
                if (item.tag == "grid")
                {
                    foreach (var column in item.columns)
                    {
                        SetFormValue(column.list, temp);
                    }
                }
                else
                {
                    temp.Add(item.id);
                }
            }
            return temp;
        }

        public static List<string> SetValueByWeb(string webForm)
        {
            var path = AppDomain.CurrentDomain.RelativeSearchPath ?? AppDomain.CurrentDomain.BaseDirectory;
            var referencedAssemblies = Directory.GetFiles(path, "*.dll").Select(Assembly.LoadFrom).ToArray();
            var t = referencedAssemblies
                .SelectMany(a => a.GetTypes().Where(t => t.FullName.Contains("Shipeng.HRMS.Domain.") && t.FullName.Contains("." + webForm + "Entity"))).First();
            List<string> temp = new List<string>();
            PropertyInfo[] pArray = t.GetProperties();
            Array.ForEach<PropertyInfo>(pArray, p =>
            {
                temp.Add(p.Name);
            });
            return temp;
        }
    }
}
