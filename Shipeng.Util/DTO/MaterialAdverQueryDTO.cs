﻿namespace Shipeng.Util.DTO
{
    public class MaterialAdverQueryDTO
    {
        /// <summary>
        /// id
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 宽度
        /// </summary>
        public int? width { get; set; }
    }
}
