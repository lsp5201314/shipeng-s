﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Shipeng.EntityFrameworkCore.Migrations
{
    [DbContext(typeof(ShipengDbContext))]
    public class DbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {

        }
    }
}
