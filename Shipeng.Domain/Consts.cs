﻿namespace Shipeng.Domain
{
    public static class Consts
    {
        /// <summary>
        /// 表前缀
        /// </summary>
        public const string DbTablePrefix = "Shipeng_";

        public const string DbSchema = null;
    }
}
