﻿namespace Shipeng.Domain.Shared.Options
{
    /// <summary>
    /// 路由转换配置项
    /// </summary>
    public class RouteTransformOption
    {
        /// <summary>
        /// 交换配置项名称
        /// </summary>
        public string TransformsName { get; set; }
        /// <summary>
        /// 交换配置项值
        /// </summary>
        public string TransformsValue { get; set; }
    }
}
