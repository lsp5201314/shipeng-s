﻿namespace Shipeng.Domain.Shared.Options
{
    public class YarpOption
    {
        /// <summary>
        /// 路由配置数据
        /// </summary>
        public List<RouteOption> Routes { get; set; }
    }
}
