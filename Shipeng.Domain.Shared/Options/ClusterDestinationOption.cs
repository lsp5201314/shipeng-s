﻿namespace Shipeng.Domain.Shared.Options
{
    public class ClusterDestinationOption
    {
        /// <summary>
        /// 目的地名称
        /// </summary>
        public string DestinationName { get; set; }
        /// <summary>
        /// 目的地地址
        /// </summary>
        public string DestinationAddress { get; set; }
    }
}
