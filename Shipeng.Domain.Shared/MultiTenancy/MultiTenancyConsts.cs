﻿namespace Shipeng.MultiTenancy
{
    public static class MultiTenancyConsts
    {
        /* 在单点轻松启用/禁用多租户
         * 如果你永远不需要多租户，你可以删除
         * 相关模块及代码部分，包括此文件
         */
        public const bool IsEnabled = true;
    }
}
