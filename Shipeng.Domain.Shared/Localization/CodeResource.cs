﻿using Volo.Abp.Localization;

namespace Shipeng.Domain.Shared.Localization
{
    [LocalizationResourceName("Code")]
    public class CodeResource
    {

    }
}