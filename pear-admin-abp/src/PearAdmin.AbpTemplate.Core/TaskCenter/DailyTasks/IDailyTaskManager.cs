﻿using Abp.Domain.Services;

namespace PearAdmin.AbpTemplate.TaskCenter.DailyTasks
{
    /// <summary>
    /// IDomainService：此接口必须由所有域服务实现，以按约定标识它们
    /// </summary>
    public interface IDailyTaskManager : IDomainService
    {
    }
}
