﻿namespace PearAdmin.AbpTemplate.Authorization.Roles
{
    public static class StaticRoleNames
    {
        /// <summary>
        /// 宿主
        /// </summary>
        public static class Host
        {
            public const string Admin = "Admin";
        }

        /// <summary>
        /// 租户
        /// </summary>
        public static class Tenants
        {
            public const string Admin = "Admin";
        }
    }
}
