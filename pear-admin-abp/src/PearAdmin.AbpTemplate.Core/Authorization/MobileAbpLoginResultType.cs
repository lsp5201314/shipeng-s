﻿namespace PearAdmin.AbpTemplate.Core.Authorization
{
    public enum MobileAbpLoginResultType : byte
    {
        /// <summary>
        /// 手机号码未验证
        /// </summary>
        UserMobilePhoneNotConfirmed = 10
    }
}
