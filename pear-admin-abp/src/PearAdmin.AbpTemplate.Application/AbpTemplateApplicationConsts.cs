﻿namespace PearAdmin.AbpTemplate
{
    public class AbpTemplateApplicationConsts
    {
        /// <summary>
        /// SimpleStringCipher解密/加密操作的默认密码短语
        /// </summary>
        public const string DefaultPassPhrase = "gsKxGZ012HLL3MI5";

        /// <summary>
        /// 默认的页面大小
        /// </summary>
        public const int DefaultPageSize = 10;

        /// <summary>
        /// 最大页面大小
        /// </summary>
        public const int MaxPageSize = 1000;

        /// <summary>
        /// 默认密码
        /// </summary>
        public const string DefaultPassword = "admin@123456";

        public const int HostId = 1;//宿主id

        /// <summary>
        /// 默认租户id
        /// </summary>
        public const int DefaultTenantId = 1;

        public const int ResizedMaxProfilPictureBytesUserFriendlyValue = 1024;

        public const int MaxProfilPictureBytesUserFriendlyValue = 5;
    }
}
